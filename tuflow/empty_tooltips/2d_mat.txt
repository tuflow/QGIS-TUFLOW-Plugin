location == Geometry Control File (.tgc)

command == Read GIS Mat

description == Used to define 2D materials (land use) inputs.

wiki link == https://wiki.tuflow.com/TUFLOW_Empty_Files#TUFLOW_Geometry_Control_file_.28TGC.29

manual link == https://docs.tuflow.com/classic-hpc/manual/2023-03/TwoD-Domains-1.html#tab:tab-twoDMaterials