location == SWMM Control File (.tscf)

command == Read GIS SWMM Inlet Usage

description == Used to define locations and attributes of inlets (pits) that connect the 2D TUFLOW and 1D SWMM models.

manual link == https://docs.tuflow.com/classic-hpc/manual/2023-03/DomainLinking-1.html#tab:tab-SWMMinletusage