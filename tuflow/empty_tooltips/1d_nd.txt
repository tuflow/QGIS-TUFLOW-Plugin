location == ESTRY Control File (.ecf)

command == Read GIS Table Links

description == Used to define 1D node locations and attributes.

wiki link == https://wiki.tuflow.com/TUFLOW_Empty_Files#ESTRY_Control_file_.28ECF.29

manual link == https://docs.tuflow.com/classic-hpc/manual/2023-03/OneD-Domains-1.html#tab:tab-DescriptionsNodes